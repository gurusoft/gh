# GuruSoft Helper - GuruSoft DevOps toolset  #

GuruSoft Helper is a DevOps enginer oriented toolset designed to speed-up the development process of docker-based microservices.

## Main features ##
* Prepare the development environment
* Versioning with auto-increment
* Wraps docker, no need to run docker directly
* Wraps git, no need to run git directly
* Wraps docker-compose, no need to tape 'docker-compose'