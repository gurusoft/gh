FROM alpine

LABEL VERSION="0.1.132"

ARG APK_CACHE_IP=172.17.0.1
ARG RUN_DEPS='git make'
ARG BUILD_DEPS=''

RUN \
 nc -z $APK_CACHE_IP 80 && echo $APK_CACHE_IP dl-cdn.alpinelinux.org >>/etc/hosts \
 ; apk --update add --no-cache $RUN_DEPS $BUILD_DEPS \
 && apk --update del $BUILD_DEPS && rm -rf /var/cache/apk/*

ADD . .

ENTRYPOINT gh

CMD version
